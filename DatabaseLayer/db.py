import sqlite3
from typing import List, Tuple


def create_db() -> None:
    """
        Initialises the db

        TO-DO: Add Exception Handling and logic in main to check if the db already exists
    """
    # STEP: Open DB connection
    db_connection = sqlite3.connect('Series.db')
    print("Opened database successfully")
    # STEP: Create STATUS table
    db_connection.execute("""CREATE TABLE STATUS
             (StatusID INT NOT NULL,
             Status CHAR(50) NOT NULL,
             PRIMARY KEY (StatusID));""")
    print("STATUS Table created successfully")
    # STEP: Add records to STATUS table
    db_connection.execute("INSERT INTO STATUS (StatusID, Status) VALUES (1, 'Plan to Watch')")
    db_connection.execute("INSERT INTO STATUS (StatusID, Status) VALUES (2, 'Watching')")
    db_connection.execute("INSERT INTO STATUS (StatusID, Status) VALUES (3, 'Completed')")
    db_connection.execute("INSERT INTO STATUS (StatusID, Status) VALUES (4, 'On Hold')")
    db_connection.commit()
    print("Added Statuses to STATUS Table")
    # STEP: Create SERIES table
    db_connection.execute("""CREATE TABLE SERIES
             (SeriesID INTEGER PRIMARY KEY,
             Title TEXT CHAR(100),
             Season INT NOT NULL,
             NumEpisodes INT NOT NULL,
             StatusID INT NOT NULL,
             FOREIGN KEY (StatusID) REFERENCES Status(StatusID));""")
    print("SERIES Table created successfully")
    # STEP: Close DB connection
    db_connection.close()
    print("Closed database successfully")

    return None


def create_test_data() -> None:
    """
        Creates dummy data for testing purposes

        TO-DO: Remove this function in final build
    """
    # STEP: Open DB connection
    db_connection = sqlite3.connect('Series.db')
    print("Opened database successfully")
    # STEP: Insert dummy records
    db_connection.execute("INSERT INTO SERIES (SeriesID, Title, Season, NumEpisodes, StatusID) \
      VALUES (1, 'The Witcher', 1, 8, 3)")
    db_connection.execute("INSERT INTO SERIES (SeriesID, Title, Season, NumEpisodes, StatusID) \
          VALUES (2, 'The Witcher', 2, 8, 2)")
    db_connection.execute("INSERT INTO SERIES (SeriesID, Title, Season, NumEpisodes, StatusID) \
          VALUES (3, 'Jojo Bizarre Adventures', 6, 14, 4)")
    db_connection.execute("INSERT INTO SERIES (SeriesID, Title, Season, NumEpisodes, StatusID) \
          VALUES (4, 'Breaking Bad', 1, 12, 1)")
    db_connection.commit()
    print("Records created successfully in SERIES Table")
    # STEP: Close DB connection
    db_connection.close()
    print("Closed database successfully")
    return None


def create_series_in_db(title: str, season: int, num_episodes: int, status_id: int) -> None:
    """
        Creates a new record in DB
    """
    try:
        # STEP: Open DB connection
        db_connection = sqlite3.connect('Series.db')
        db_cursor = db_connection.cursor()
        print("Opened database successfully")
        # STEP: Insert in DB
        insert_query = "INSERT INTO SERIES (Title, Season, NumEpisodes, StatusID) \
          VALUES ('{}', {}, {}, {});".format(title, season, num_episodes, status_id)

        db_cursor.execute(insert_query)
        db_connection.commit()
        print(db_cursor.rowcount, "Record inserted successfully into SERIES table")
        # STEP: Close DB connection
        db_cursor.close()
        db_connection.close()
        print("Closed database successfully")

    except sqlite3.Error as error:
        print("Failed to insert record: {}".format(error))

    return None


# def fetch_series_in_db(series_id: int) -> Tuple:
#     """
#         Retrieves a record from DB.
#
#         Not currently in use.
#     """
#     try:
#         # STEP: Open DB connection
#         db_connection = sqlite3.connect('Series.db')
#         db_cursor = db_connection.cursor()
#         print("Opened database successfully")
#         # STEP: Select from DB
#         select_query = "SELECT * FROM SERIES WHERE (SeriesID = {});".format(series_id)
#
#         db_cursor.execute(select_query)
#         series_record = db_cursor.fetchone()
#         print("1 record selected from table SERIES")
#         # STEP: Close DB connection
#         db_cursor.close()
#         db_connection.close()
#         print("Closed database successfully")
#
#         return series_record
#
#     except sqlite3.Error as error:
#         print("Failed to get record: {}".format(error))
#
#     return ()


def fetch_status_in_db(status_id: int) -> str:
    """
        Retrieves Status value from DB for a given StatusID
    """
    try:
        # STEP: Open DB connection
        db_connection = sqlite3.connect('Series.db')
        db_cursor = db_connection.cursor()
        print("Opened database successfully")
        # STEP: Select Status from db
        select_query = "SELECT Status FROM STATUS WHERE (StatusID = {});".format(status_id)

        db_cursor.execute(select_query)
        status = db_cursor.fetchone()[0]
        print("Status Fetched")
        # STEP: Close DB connection
        db_cursor.close()
        db_connection.close()
        print("Closed database successfully")

        return status

    except sqlite3.Error as error:
        print("Failed to get status: {}".format(error))

    return ""


def select_all_series_in_db() -> List:
    """
        Retrieves all records from DB and returns a list of tuples
    """
    try:
        # STEP: Open DB connection
        db_connection = sqlite3.connect('Series.db')
        db_cursor = db_connection.cursor()
        print("Opened database successfully")
        # STEP: Select all records from DB
        select_query = "SELECT * FROM SERIES;"

        db_cursor.execute(select_query)
        series_records = db_cursor.fetchall()
        print("Total number of rows in table: ", len(series_records))
        # STEP: Close DB connection
        db_cursor.close()
        db_connection.close()
        print("Closed database successfully")

        return series_records

    except sqlite3.Error as error:
        print("Failed to get records: {}".format(error))

    return []


def update_series_in_db(series_id: int, title: str, season: int, num_episodes: int, status_id: int) -> None:
    """
        Updates a record in DB
    """
    try:
        # STEP: Open DB connection
        db_connection = sqlite3.connect('Series.db')
        db_cursor = db_connection.cursor()
        print("Opened database successfully")
        # STEP: Create Update queries for each attribute
        title_update_query = "UPDATE SERIES SET Title = '{}' WHERE (SeriesID = {});".format(title, series_id)
        season_update_query = "UPDATE SERIES SET Season = {} WHERE (SeriesID = {});".format(season, series_id)
        num_episodes_update_query = "UPDATE SERIES SET NumEpisodes = {} WHERE (SeriesID = {});".format(num_episodes, series_id)
        status_id_update_query = "UPDATE SERIES SET StatusID = {} WHERE (SeriesID = {});".format(status_id, series_id)
        # STEP: Execute Update queries
        db_cursor.execute(title_update_query)
        db_cursor.execute(season_update_query)
        db_cursor.execute(num_episodes_update_query)
        db_cursor.execute(status_id_update_query)
        db_connection.commit()
        print("Updated 1 record in Table SERIES")
        # STEP: Close DB connection
        db_cursor.close()
        db_connection.close()
        print("Closed database successfully")

    except sqlite3.Error as error:
        print("Failed to update record: {}".format(error))

    return None


def delete_series_in_db(series_id: int) -> None:
    """
        Deletes a record in DB
    """
    try:
        # STEP: Open DB connection
        db_connection = sqlite3.connect('Series.db')
        db_cursor = db_connection.cursor()
        print("Opened database successfully")
        # STEP: Delete record from DB
        delete_query = "DELETE FROM SERIES WHERE SeriesID = {};".format(series_id)

        db_cursor.execute(delete_query)
        db_connection.commit()
        print("Deleted 1 record from table SERIES")
        # STEP: Close DB connection
        db_cursor.close()
        db_connection.close()
        print("Closed database successfully")

    except sqlite3.Error as error:
        print("Failed to delete record: {}".format(error))

    return None
