import kivy
from kivy.app import App
# from kivy.base import runTouchApp
# from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.recycleview import RecycleView
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.dropdown import DropDown
from kivy.properties import StringProperty, ListProperty

from BusinessLayer import business
from BusinessLayer.classes import SeriesObject
# TO-DO: Remove after testing
# from DatabaseLayer import db

kivy.require('2.0.0')


class MainPage(BoxLayout):
    """
        Widget for App landing/main page
    """
    def __init__(self, **kwargs):
        super(MainPage, self).__init__(**kwargs)


class Header(BoxLayout):
    """
        Child widget of MainPage
    """
    def __init__(self, **kwargs):
        super(Header, self).__init__(**kwargs)


class Body(BoxLayout):
    """
        Child widget of MainPage
    """
    def __init__(self, **kwargs):
        super(Body, self).__init__(**kwargs)


class ViewRow(BoxLayout):
    """
        Widget to generate each row of the main dataset view
    """
    title = StringProperty()
    season = StringProperty()
    num_episodes = StringProperty()
    status = StringProperty()

    def __init__(self, **kwargs):
        super(ViewRow, self).__init__(**kwargs)


class AddSeriesPopup(Popup):
    """
        Widget that generates the AddSeries window when AddSeries button is selected
    """
    def __init__(self, **kwargs):
        super(AddSeriesPopup, self).__init__(**kwargs)

    def add_series_button(self):
        title = self.ids['title_input'].text
        season = self.ids['season_input'].text
        num_episodes = self.ids['num_episodes_input'].text
        status = self.ids['status_button'].text
        series = SeriesObject(series_id=0, title=title, season=season, num_episodes=num_episodes, status=status)

        business.create_series_in_db_handler(series)


# class EditSeriesPopup(Popup):
#     def __init__(self, **kwargs):
#         super(EditSeriesPopup, self).__init__(**kwargs)
#
#     series_id = NumericProperty()
#
#     def edit_series_button(self, series_id):
#         title = self.ids['title_input'].text
#         season = self.ids['season_input'].text
#         num_episodes = self.ids['num_episodes_input'].text
#         status = self.ids['status_button'].text
#         series = SeriesObject(series_id=series_id, title=title, season=season, num_episodes=num_episodes, status=status)
#
#         business.update_series(series, title)


class MainSeriesView(BoxLayout):
    """
        Child widget of Body. Renders the main dataset view
    """
    series_list = ListProperty()

    def __init__(self, **kwargs):
        super(MainSeriesView, self).__init__(**kwargs)
        self.series_list = business.get_all_series()

    def update_dataset(self):
        """
            Function to update the app dataset with the latest state of the database
        """
        self.series_list = business.get_all_series()


class StatusDropdown(DropDown):
    """
        Widget to display Status dropdown list
    """
    def __init__(self, **kwargs):
        super(StatusDropdown, self).__init__(**kwargs)


class SeriesTrackerApp(App):
    """
        Main Kivy App Class
    """
    def build(self):
        home_page = MainPage()
        return home_page


if __name__ == '__main__':
    # # TO-DO: Testing stuff. Remove on final build
    # db.create_db()
    # db.create_test_data()
    # series1 = SeriesObject(5, 'GoT', 1, 1, 'Completed')
    # business.delete_series(series1)
    # series2 = {'series_id': 4, 'title': 'Breaking Bad', 'season': 1, 'num_episodes': 12, 'status': 'Watching'}
    # business.update_series(series2, 'Breaking Good', 2, 16, 'On Hold')
    SeriesTrackerApp().run()
