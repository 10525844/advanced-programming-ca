from DatabaseLayer import db
from BusinessLayer.classes import SeriesObject, STATUS
from typing import List, Dict


def create_series_in_db_handler(series: SeriesObject) -> None:
    db.create_series_in_db(series.title, series.season, series.num_episodes, series.status_id)
    return None


def select_all_series_in_db_handler() -> List[List]:
    all_series_db_records = db.select_all_series_in_db()
    return all_series_db_records


def get_all_series() -> List[Dict]:
    """
        Retrieves and returns a list of dictionaries of all records in DB
    """
    all_series_list = []
    all_series_from_db = select_all_series_in_db_handler()

    # STEP: Convert tuples from db into dictionaries
    for series in all_series_from_db:
        series_id = series[0]
        title = series[1]
        season = series[2]
        num_episodes = series[3]
        status = fetch_status_handler(series[4])

        series_obj = SeriesObject(series_id, title, season, num_episodes, status)
        all_series_list.append(convert_series_obj_to_dict(series_obj))
    return all_series_list


def convert_series_obj_to_dict(series_obj: SeriesObject) -> Dict:
    """
        Function to convert a SeriesObject to a dictionary
    """
    series = {
        "series_id": series_obj.series_id,
        "title": series_obj.title,
        "season": str(series_obj.season),
        "num_episodes": str(series_obj.num_episodes),
        "status": series_obj.status,
        "status_id": series_obj.status_id,
    }
    return series


def convert_dict_to_series_obj(series_dict: Dict) -> SeriesObject:
    """
        Function to convert a dictionary to a SeriesObject
    """
    series_id = series_dict['series_id']
    title = series_dict['title']
    season = series_dict['season']
    num_episodes = series_dict['num_episodes']
    status = series_dict['status']
    series = SeriesObject(series_id=series_id, title=title, season=season, num_episodes=num_episodes, status=status)

    return series


def fetch_status_handler(status_id: int) -> str:
    status = db.fetch_status_in_db(status_id)
    return status


def update_series_in_db_handler(series: SeriesObject) -> None:
    db.update_series_in_db(series.series_id, series.title, series.season, series.num_episodes, series.status_id)
    return None


def update_series(series_dict: Dict, new_title: str, new_season: int,
                  new_num_episodes: int, new_status: str) -> None:
    """
        Updates a series record in the database
    """
    new_status_id = fetch_status_id(new_status)
    series = convert_dict_to_series_obj(series_dict)

    series.title = new_title
    series.season = new_season
    series.num_episodes = new_num_episodes
    series.status_id = new_status_id

    update_series_in_db_handler(series)
    return None


def fetch_status_id(status: str) -> int:
    """
        Retrieves and returns the corresponding status_id for a given status
    """
    status_id = STATUS[status]
    return status_id


def delete_series_in_db_handler(series: SeriesObject) -> None:
    db.delete_series_in_db(series.series_id)
    return None


def delete_series(series: SeriesObject) -> None:
    """
            Function to delete a record from database
    """
    db.delete_series_in_db(series.series_id)
    return None
