STATUS = {
    "Plan to Watch": 1,
    "Watching": 2,
    "Completed": 3,
    "On Hold": 4
}


class SeriesObject:
    def __init__(self, series_id: int, title: str, season: int, num_episodes: int, status: str):
        self.series_id = series_id
        self.title = title
        self.season = season
        self.num_episodes = num_episodes
        self.status = status
        self.status_id = STATUS[status]
